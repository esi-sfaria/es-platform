const webpack = require('webpack');
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const dotenv = require('dotenv').config({ path: __dirname + '/../../.env' });

module.exports = (env) => {
    var config = {
        entry: ['react-hot-loader/patch', './src/index.js'],
        mode: 'development',

        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'main.js',
        },

        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: 'index.html',
            }),
            new webpack.DefinePlugin({
              'process.env.SHOPIFY_API_KEY': JSON.stringify(dotenv.parsed.SHOPIFY_API_KEY),
              'process.env.SHOPIFY_API_SECRET': JSON.stringify(dotenv.parsed.SHOPIFY_API_SECRET),
              'process.env.SHOP': JSON.stringify(dotenv.parsed.SHOP),
              'process.env.SCOPES': JSON.stringify(dotenv.parsed.SCOPES),
              'process.env.HOST': JSON.stringify(dotenv.parsed.HOST),
              'process.env.APPLICATION_URL': JSON.stringify(dotenv.parsed.APPLICATION_URL)
            })
        ],

        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                            plugins: ['react-hot-loader/babel', "@babel/plugin-transform-runtime"],
                        },
                    },
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: ['file-loader'],
                },
            ],
        },
    };
    if (env.development) {
        config.devServer = {
            port: 4000,
            https: true,
            static: './dist',
            hot: true,
            allowedHosts: "all",
            host: 'localhost',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
            },
        };
        config.devtool = 'inline-source-map';
        config.mode = 'development';
    }
    return config;
};
