import React from 'react';
import { AppProvider } from "@shopify/polaris";
import { Provider, useAppBridge } from "@shopify/app-bridge-react";
import { authenticatedFetch } from "@shopify/app-bridge-utils";
import { Redirect } from "@shopify/app-bridge/actions";
import "@shopify/polaris/dist/styles.css";
import translations from "@shopify/polaris/locales/en.json";
import { getSessionToken } from "@shopify/app-bridge-utils";

function userLoggedInFetch(app) {
  const fetchFunction = authenticatedFetch(app);

  return async (uri, options) => {
    const response = await fetchFunction(uri, options);

    if (
      response.headers.get("X-Shopify-API-Request-Failure-Reauthorize") === "1"
    ) {
      const authUrlHeader = response.headers.get(
        "X-Shopify-API-Request-Failure-Reauthorize-Url"
      );

      const redirect = Redirect.create(app);
      redirect.dispatch(Redirect.Action.APP, authUrlHeader || `/auth`);
      return null;
    }

    return response;
  };
}

async function MyProvider(props) {
  const app = useAppBridge();
  getSessionToken(app).then(ctx => console.log(ctx));
  userLoggedInFetch(app)('/auth', { credentials: "include" }).then(ctx => console.log(ctx));
  app.getState().then((ctx) => console.log(ctx));

  const Component = props.Component;

  return <Component {...props} />;
}

class MyApp extends React.Component {
  render() {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let host = params.get('host');
    const { Component, ...props } = this.props;
    return (
      <AppProvider i18n={translations}>
        <Provider
          config={{
            apiKey: process.env.SHOPIFY_API_KEY,
            host: host,
            forceRedirect: true,
          }}
        >
          <MyProvider Component={Component} {...props} />
        </Provider>
      </AppProvider>
    );
  }
}

export default MyApp;
