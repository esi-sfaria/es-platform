import React from 'react';
import ReactDOM from 'react-dom';
import Index from './pages/index';
import MyApp from './pages/_app';

ReactDOM.render(<MyApp Component={Index} />, document.getElementById('root'));
