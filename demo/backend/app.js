require('isomorphic-fetch');
const Koa = require('koa');
const { default: createShopifyAuth, verifyRequest } = require('@shopify/koa-shopify-auth');
// const session = require('koa-session');
// const awsServerlessKoaMiddleware = require('aws-serverless-koa/middleware');
const { SHOPIFY_API_SECRET, SHOPIFY_API_KEY, APPLICATION_URL, SCOPES, HOST } = process.env;
const { Shopify, ApiVersion } = require("@shopify/shopify-api");
const Router = require("koa-router");
const next = require("next");
const dev = process.env.NODE_ENV !== "production";
const server = next({
  dev,
});
const handle = server.getRequestHandler();

Shopify.Context.initialize({
  API_KEY: SHOPIFY_API_KEY,
  API_SECRET_KEY: SHOPIFY_API_SECRET,
  SCOPES: SCOPES.split(","),
  HOST_NAME: HOST.replace(/https:\/\/|\/$/g, ""),
  API_VERSION: ApiVersion.October20,
  IS_EMBEDDED_APP: true,
  SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
});

const ACTIVE_SHOPIFY_SHOPS = {};

let app = new Koa();
let router = new Router();
app.keys = [Shopify.Context.API_SECRET_KEY];
app.use(
  createShopifyAuth({
    async afterAuth(ctx) {
      // Access token and shop available in ctx.state.shopify
      const { shop, accessToken, scope } = ctx.state.shopify;
      const host = ctx.query.host;
      ACTIVE_SHOPIFY_SHOPS[shop] = scope;

      const response = await Shopify.Webhooks.Registry.register({
        shop,
        accessToken,
        path: "/webhooks",
        topic: "APP_UNINSTALLED",
        webhookHandler: async (topic, shop, body) =>
          delete ACTIVE_SHOPIFY_SHOPS[shop],
      });

      if (!response.success) {
        console.log(
          `Failed to register APP_UNINSTALLED webhook: ${response.result}`
        );
      }

      // Redirect to app with shop parameter upon auth
      ctx.redirect(`/?shop=${shop}&host=${host}`);
    },
  })
);

router.post("/webhooks", async (ctx) => {
  try {
    await Shopify.Webhooks.Registry.process(ctx.req, ctx.res);
    console.log(`Webhook processed, returned status code 200`);
  } catch (error) {
    console.log(`Failed to process webhook: ${error}`);
  }
});

router.post(
  "/graphql",
  verifyRequest({ returnHeader: true }),
  async (ctx, next) => {
    await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
  }
);

router.get(
  "/auth",
  verifyRequest({ returnHeader: true }),
  async (ctx, next) => {
    ctx.body = "Works!"
  }
);

const handleRequest = async (ctx) => {
  console.log(ctx.req);
  await handle(ctx.req, ctx.res);
  ctx.respond = false;
  ctx.res.statusCode = 200;
};

router.get("(.*)", handleRequest);

app.use(router.allowedMethods());
app.use(router.routes());

module.exports = app;
