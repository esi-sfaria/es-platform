const webpack = require('webpack');
const slsw = require('serverless-webpack');
let dotenv = require('dotenv').config({ path: __dirname + '/../../.env' });

module.exports = {
  entry: slsw.lib.entries,
  target: 'node',
  mode: 'development',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.SHOPIFY_API_KEY': JSON.stringify(dotenv.parsed.SHOPIFY_API_KEY),
      'process.env.SHOPIFY_API_SECRET': JSON.stringify(dotenv.parsed.SHOPIFY_API_SECRET),
      'process.env.SHOP': JSON.stringify(dotenv.parsed.SHOP),
      'process.env.SCOPES': JSON.stringify(dotenv.parsed.SCOPES),
      'process.env.HOST': JSON.stringify(dotenv.parsed.HOST),
      'process.env.APPLICATION_URL': JSON.stringify(dotenv.parsed.APPLICATION_URL)
    })
  ],
};
